package ru.t1.chubarov.tm.exception.field;

public final class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException(String login, String email) {
        super("Error. Email " + email + "is empty. For login=" + login);
    }

}
