package ru.t1.chubarov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.endpoint.*;
import ru.t1.chubarov.tm.api.service.ITokenService;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.enumerated.TaskSort;
import ru.t1.chubarov.tm.marker.SoapCategory;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;
import ru.t1.chubarov.tm.dto.model.TaskDTO;
import ru.t1.chubarov.tm.service.TokenService;

import java.util.List;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    private static int NUMBER_OF_ENTRIES = 3;

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @Nullable
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final ITokenService tokenService = new TokenService();

    @Nullable
    private String token;

    @Nullable
    private String taskId;

    @Before
    public void initTest() {
        @NotNull final String login = "user";
        @NotNull final String password = "user";
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        request.setLogin(login);
        request.setPassword(password);
        token = authEndpoint.login(request).getToken();
        tokenService.setToken(token);

        @NotNull final TaskClearRequest requestClear = new TaskClearRequest(token);
        taskEndpoint.clearTask(requestClear);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final String name = "Task Name " + i;
            @NotNull final String description = "description test " + i;
            @NotNull final TaskCreateRequest taskrequest = new TaskCreateRequest(name, description, token);
            if (i == 1) taskId = taskEndpoint.createTask(taskrequest).getTask().getId();
            else taskEndpoint.createTask(taskrequest);
        }
    }

    @Test
    public void testSize() {
        @NotNull final TaskSort sort = TaskSort.toSort("BY_NAME");
        @NotNull final TaskListRequest request = new TaskListRequest(sort, token);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTask(request).getTasks();
        Assert.assertEquals(NUMBER_OF_ENTRIES, tasks.size());

    }

    @Test
    public void testCreateTask() {
        @NotNull final String name = "New Task Name";
        @NotNull final String description = "New description test";
        taskEndpoint.createTask(new TaskCreateRequest(name, description, token));

        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTask(new TaskListRequest(TaskSort.toSort("BY_NAME"), token)).getTasks();
        Assert.assertEquals("Task Name 1", tasks.get(0).getName());
        Assert.assertEquals(name, tasks.get(3).getName());
        Assert.assertEquals(description, tasks.get(3).getDescription());
        Assert.assertEquals(Status.NOT_STARTED, tasks.get(3).getStatus());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, tasks.size());
        Assert.assertThrows(
                Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest("", description, token)));
        Assert.assertThrows(
                Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest(null, description, token)));
        Assert.assertThrows(
                Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest(name, null, token)));
        Assert.assertThrows(
                Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest(name, description, null)));
    }

    @Test
    public void testListTask() {
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTask(new TaskListRequest(TaskSort.toSort("BY_NAME"), token)).getTasks();
        Assert.assertEquals("Task Name 1", tasks.get(0).getName());
        Assert.assertEquals("Task Name 2", tasks.get(1).getName());
        Assert.assertEquals("Task Name 3", tasks.get(2).getName());
        Assert.assertEquals(NUMBER_OF_ENTRIES, tasks.size());
        @Nullable final List<TaskDTO> tasks2 = taskEndpoint.listTask(new TaskListRequest(null, token)).getTasks();
        Assert.assertEquals("Task Name 1", tasks2.get(0).getName());
        Assert.assertEquals("Task Name 2", tasks2.get(1).getName());
        Assert.assertEquals("Task Name 3", tasks2.get(2).getName());
    }

    @Test
    public void testSClear() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskListRequest request = new TaskListRequest(TaskSort.toSort("BY_NAME"), token);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTask(request).getTasks();
        Assert.assertNull(tasks);
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(token);
        request.setId(taskId);
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final TaskDTO task = taskEndpoint.changeTaskStatusById(request).getTask();
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
        request.setId(taskId);
        request.setStatus(Status.COMPLETED);
        @Nullable final TaskDTO task_complete = taskEndpoint.changeTaskStatusById(request).getTask();
        Assert.assertEquals(Status.COMPLETED, task_complete.getStatus());
        request.setStatus(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(request));
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final List<TaskDTO> tasks0 = taskEndpoint.listTask(new TaskListRequest(TaskSort.toSort("BY_NAME"), token)).getTasks();
        Assert.assertEquals(NUMBER_OF_ENTRIES, tasks0.size());
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(token);
        request.setId(taskId);
        taskEndpoint.removeTaskById(request);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTask(new TaskListRequest(TaskSort.toSort("BY_NAME"), token)).getTasks();
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, tasks.size());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(null));
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(request));
    }

    @Test
    public void testGetById() {
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(token);
        request.setId(taskId);
        @NotNull final TaskDTO task = taskEndpoint.getTaskById(request).getTask();
        Assert.assertEquals("Task Name 1", task.getName());
        Assert.assertEquals("description test 1", task.getDescription());
        Assert.assertEquals(taskId, task.getId());
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskById(request));
        request.setId("failId");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskById(request));
    }

    @Test
    public void testUpdateById() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(token);
        request.setId(taskId);
        request.setName("New Task Name");
        request.setDescription("New Task Description");
        taskEndpoint.updateTaskById(request);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTask(new TaskListRequest(TaskSort.toSort("BY_NAME"), token)).getTasks();
        Assert.assertEquals("New Task Name", tasks.get(0).getName());
        Assert.assertEquals("New Task Description", tasks.get(0).getDescription());
        Assert.assertEquals(NUMBER_OF_ENTRIES, tasks.size());

        request.setId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(request));
        request.setId("fail_Id");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(request));
        request.setId(taskId);
        request.setName("");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(request));
        request.setName(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(request));
        request.setName("New Task Name");
        request.setDescription("");
        taskEndpoint.updateTaskById(request);
        @Nullable final List<TaskDTO> tasks_desc = taskEndpoint.listTask(new TaskListRequest(TaskSort.toSort("BY_NAME"), token)).getTasks();
        Assert.assertEquals("New Task Name", tasks_desc.get(0).getName());
        Assert.assertEquals("", tasks_desc.get(0).getDescription());
    }

    @Test
    public void testGetByProjectId() {
        @NotNull final ProjectClearRequest requestClear = new ProjectClearRequest(token);
        projectEndpoint.clearProject(requestClear);
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(token);
        requestCreate.setName("ProjectNew");
        requestCreate.setDescription("Project for test showT ask by project");
        @Nullable final ProjectDTO project = projectEndpoint.createProject(requestCreate).getProject();

        @NotNull final ProjectTaskBindToProjectRequest requestBind = new ProjectTaskBindToProjectRequest(token);
        requestBind.setProjectId(project.getId());
        requestBind.setTaskId(taskId);
        projectEndpoint.bindTaskToProject(requestBind);

        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(token);
        request.setProjectId(project.getId());
        @NotNull final List<TaskDTO> tasks = taskEndpoint.listTaskByProjectId(request).getTasks();
        Assert.assertEquals("Task Name 1", tasks.get(0).getName());
        Assert.assertEquals("description test 1", tasks.get(0).getDescription());
        Assert.assertEquals(taskId, tasks.get(0).getId());
        Assert.assertEquals(1, tasks.size());
    }

}
