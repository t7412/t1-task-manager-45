package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.dto.model.UserDTO;
import ru.t1.chubarov.tm.repository.dto.UserDtoRepository;
import ru.t1.chubarov.tm.service.ConnectionService;
import ru.t1.chubarov.tm.service.PropertyService;
import ru.t1.chubarov.tm.util.HashUtil;

import javax.persistence.EntityManager;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 1;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);

    @NotNull
    private final String userLogin = "TestLogin";

    @NotNull
    private final String userEmail = "Test@mail.test";

    @SneakyThrows
    @Before
    public void initRepository() {
        propertyService = new PropertyService();
        entityManager.getTransaction().begin();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash(HashUtil.salt(propertyService, "password"));
        user.setRole(Role.USUAL);
        userRepository.add(user);
        entityManager.getTransaction().commit();
    }

    @After
    public void finish() throws Exception {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(userLogin);
        entityManager.getTransaction().begin();
        userRepository.remove(user);
        userRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Test
    public void testFindByLogin() throws Exception {
        Assert.assertEquals(userLogin, userRepository.findOneById(userLogin).getLogin());
    }

    @Test
    public void testfindByEmail() throws Exception {
        Assert.assertEquals(userLogin, userRepository.findOneById(userEmail).getLogin());
    }

    @Test
    public void testisLoginExist() throws Exception {
        Assert.assertTrue(userRepository.isLoginExist(userLogin));
        Assert.assertFalse(userRepository.isLoginExist("Login"));
    }

    @Test
    public void testisEmailExist() throws Exception {
        Assert.assertTrue(userRepository.isEmailExist(userEmail));
        Assert.assertFalse(userRepository.isEmailExist("user@Email.org"));
    }

    @Test
    public void testProfile() throws Exception {
        @NotNull final UserDTO user = userRepository.findByLogin(userLogin);
        @NotNull final String userId = user.getId();
        @NotNull final String newLogin = "new_login";
        @NotNull final String newPassword = "new_password";
        user.setFirstName("Ivanov");
        user.setLastName("Piter");
        user.setMiddleName("Vasilech");
        user.setLogin(newLogin);
        user.setPasswordHash(HashUtil.salt(propertyService, newPassword));
        entityManager.getTransaction().begin();
        userRepository.update(user);
        entityManager.getTransaction().commit();
        Assert.assertNull(userRepository.findByLogin(userLogin));
        @NotNull final UserDTO updated_user = userRepository.findByLogin(newLogin);
        Assert.assertEquals(userId, updated_user.getId());
        Assert.assertEquals("Ivanov", updated_user.getFirstName());
        Assert.assertEquals("Piter", updated_user.getLastName());
        Assert.assertEquals("Vasilech", updated_user.getMiddleName());
        Assert.assertEquals(HashUtil.salt(propertyService, newPassword), updated_user.getPasswordHash());
    }

    @Test
    public void testSetLock() throws Exception {
        @NotNull final UserDTO user = userRepository.findByLogin(userLogin);
        Assert.assertFalse(userRepository.findByLogin(userLogin).getLocked());
        user.setLocked(true);
        userRepository.update(user);
        Assert.assertTrue(userRepository.findByLogin(userLogin).getLocked());
        user.setLocked(false);
        userRepository.update(user);
        Assert.assertFalse(userRepository.findByLogin(userLogin).getLocked());
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
        userRepository.add(new UserDTO());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userRepository.getSize());
    }

    @Test
    public void testClear() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
        userRepository.clear();
        Assert.assertEquals(0, userRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemove() {
        @NotNull final UserDTO user_empty = userRepository.findByLogin("emptyLogin");
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
        @NotNull final UserDTO user = userRepository.findByLogin(userLogin);
        userRepository.remove(user);
        Assert.assertEquals(0, userRepository.getSize());
    }

}
