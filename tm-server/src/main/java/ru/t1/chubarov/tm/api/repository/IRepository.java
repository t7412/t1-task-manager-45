package ru.t1.chubarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModelDTO> {

    @NotNull
    M add(@Nullable M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

    void clear() throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @Nullable
    List<M> findAll(@Nullable Comparator<M> comparator) throws  Exception;

    @Nullable
    M findOneById(@NotNull  String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull  Integer index) throws Exception;

    @NotNull
    M remove(@NotNull M model) throws Exception;

    int getSize() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

}
