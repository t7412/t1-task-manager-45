package ru.t1.chubarov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.repository.model.IModelRepository;
import ru.t1.chubarov.tm.model.AbstractModel;

import javax.persistence.EntityManager;

public abstract class AbstractRepository<M extends AbstractModel> implements IModelRepository<M> {

    @NotNull
    public final EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
