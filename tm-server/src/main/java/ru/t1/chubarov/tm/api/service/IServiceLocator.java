package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.service.dto.IProjectDtoService;
import ru.t1.chubarov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.chubarov.tm.api.service.dto.ITaskDtoService;
import ru.t1.chubarov.tm.api.service.dto.IUserDtoService;

public interface IServiceLocator {

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    IPropertyService getPropertyService();

    @Nullable
    ITaskDtoService getTaskService();

    @Nullable
    IProjectTaskDtoService getProjectTaskService();

    @Nullable
    ILoggerService getLoggerService();

    @Nullable
    abstract IUserDtoService getUserService();

    @Nullable
    IAuthService getAuthService();

    @Nullable
    IDomainService getDomainService();

    @Nullable
    ICommandService getCommandService();

}
