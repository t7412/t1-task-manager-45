package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ISaltProvider {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getServerPort();

}
